package com.example.andrey.service;

import com.example.andrey.domain.Message;
import com.example.andrey.domain.User;
import com.example.andrey.repos.MessageRepos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
  @Autowired
  private MessageRepos messageRepos;

  public Page<Message> messageList(Pageable pageable, String filter){
    if (filter != null && !filter.isEmpty()) {
      return messageRepos.findByTag(filter, pageable);
    } else{
      return messageRepos.findAll(pageable);
    }
  }

  public Page<Message> messageListForUser(Pageable pageable, User currentUser, User author) {
    return messageRepos.findByUser(pageable, author);
  }
}
